---
layout: page
title: Archive
permalink: /archive/
---

<ul class="list-group-item">
  {% for post in site.posts %}
    {% unless post.next %}
      <h2 class="tag">{{ post.date | date: '%Y' }}</h2>
    {% else %}
      {% capture year %}
        {{ post.date | date: '%Y' }}
      {% endcapture %}
      {% capture nyear %}
        {{ post.next.date | date: '%Y' }}
      {% endcapture %}
      {% if year != nyear %}
        <h3>{{ post.date | date: '%Y' }}</h3>
      {% endif %}
    {% endunless %}

    <li class="list-group-item">{{ post.date | date:"%b %d" }} - <a href="{{ site.baseurl }}{{ post.url }}">{{ post.title }}</a>&nbsp;&nbsp;&nbsp;[
    {% assign post = page %}
    {% if post.tags.size > 0 %}
      {% capture tags_content %}
        {% if post.tags.size == 1 %}
        <i class="fa fa-tag"></i>
    {% else %}
        <i class="fa fa-tags"></i>
    {% endif %}
    {% endcapture %}
    {% for post_tag in post.tags %}
      {% for data_tag in site.data.tags %}
        {% if data_tag.slug == post_tag %}
          {% assign tag = data_tag %}
        {% endif %}
      {% endfor %}
      {% if tag %}
        {% capture tags_content_temp %}
          {{ tags_content }}
          <a href="{{ site.baseurl }}/resources/tag/{{ tag.slug }}/">{{ tag.name }}</a>
          {% if forloop.last == false %},
          {% endif %}
	{% endcapture %}
        {% assign tags_content = tags_content_temp %}
      {% endif %}
    {% endfor %}
  {% else %}
    {% assign tags_content = '' %}
  {% endif %}
  {{tags_content}}
  ]</li>
{% endfor %}
</ul>