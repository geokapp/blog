---
layout: post
title: Hello World!
comments: true
tags: [Intro]
---

Who am I
--------
My name is Giorgos Kappes. I am a computer scientist with a MSc degree in computer systems. Currently, I am working on my PhD at the [Department of Computer Science and Engineering][1] in the [University of Ioannina][2]. My main research interests include some hot topics of computer systems, as: Operating Systems, Virtualization, File & Storage Systems, and Systems Security.

I was born in [Ioannina][3], a beautiful city of Greece that uniquely combines history and tradition with modern living.  I graduated the [2nd High School of Ioannina][4] and I attended the [Department of Computer Science][1] of the [University of Ioannina][2] from which I obtained my BSc and MSc degrees.

My first touch with computers was back in the late nineties when my father brought home an old [Samsung SPC-3000V][5] PC running PC-DOS. Suprisingly, its Intel 8088, and 640 KB of RAM were more than enough to let me enter the magic world of programming.  My first "program" was a DOS Batch script and the first programming languages that I learned were Logo and Quick Basic. 

Apart from computers, I enjoy riding my road and mountain bicycles. My favorite route is to ride around the lake of Ioannina. I ride this route since I was six years old. I also love swimming and visiting new places.

[1]: http://cs.uoi.gr/
[2]: http://www.uoi.gr/
[3]: http://www.ioannina.gr/ 
[4]: http://2lykioannina.gr/
[5]: http://electricdreams.hopto.org/comp/sam3v.php?current=2&s1=1
