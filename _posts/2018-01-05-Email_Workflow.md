---
layout: post
title: Thunderbird as the ultimate personal organization system
comments: true
tags: [Email,Thunderbird,Gmail]
---

In this blog bost I will describe  how I combined the strengths of the Thunderbird email client and Gmail to manage email messages more efficiently. 

## Setup

I use thunderbird in conjunction with Gmail and it seems that they are working nicely together. I have configured an IMAP account that synchronizes my messages with Gmail and an SMTP account to send messages from thunderbird using Gmail's SMTP. You can find a nice guide for configuring thunderbird with Gmail [here](https://support.mozilla.org/el/kb/thunderbird-and-Gmail).

## Workflow

My messages are organized in two steps. The former is a an automatic categorization stage which is performed automatically by Gmail. The later is a combination of an automatic and manual tagging stage which is performed in thunderbird.

### 1st step: Gmail categorization

As all the messages reach Gmail first, I will firstly focus on the configuration that I have performed on my Gmail account to achieve an automatic categorization of all my received messages. To perform the automatic categorization I rely on the *filters* and *smartlabels* tools that Gmail provides. You can find the filters at the *filters and blocked addresses* tab under *settings*. To use the smartlabels tool you have to enable it first. You can find it at the *labs* tab under *settings*.

To begin with, I decided to separate the messages into two groups. The first group contains messages that need to be reviewed as soon as possible, while the later contains messages that can be reviewed later. This separation is needed in order to stop the messages of the second group from distracting me when they received.

To achieve this separation, I created a label called *Auto Read* in Gmail. I also created a label for each category, *Updates*, *Forums*, *Social*, *Promotions*, *Finance*, *Travel*. To classify the messages automatically, I created some filters that put the *Auto Read* label on all the received messages that are categorized by Gmail as social, promotions, forums and updates, and mark them as read. These filters also label each message with the label of its its category. All these messages belong in the second group of messages that do not need immediate review. For the messages that belong to the remaining categories, *Finance* and *Travel*, I created the respective filters that label each received message with the label of its category. The messages that are uncategorized are not labeled. All these messages belong in the first group and are marked as unread in the inbox.

The effect of this separation is a less distracting inbox with only the important new messages marked as unread. The *Auto Read* label on all the other messages allows me to review them when I have the time to do so.

### 2nd step: thunderbird tagging

Gmail provides the notion of labels which can be viewed as a combination of tags and virtual folders. Gmail uses the *All mail* folder to store all the messages, but when you put a label on a message, this message is virtually copied into the virtual folder that corresponds to the label. If you put multiple labels on each message, then the message will appear on the virtual folder of each label. On the other hand, thunderbird supports real folders and tags. Each Gmail label is transformed into a thunderbird folder. You can find more on this subject [here](https://support.mozilla.org/el/kb/thunderbird-and-Gmail#w_understanding-Gmail-labels-and-thunderbird-folders).

I use both folders and tags to organize my messages locally. Each message is classified into the folder that corresponds to its category as it is received by thunderbird. The second step of message organization uses thunderbird's filtering mechanism to automatically tag each message with one of the following tags: *Updates*, *Forums*, *Social*, *Promotions*, *Finance*, *Travel*. The remaining messages that do not belong on any of these categories are either tagged as *Personal* (if the senders belong in my personal contacts) or *Primary* if they have no other tags applied.

Finally, I have defined a set of tags that I use to tag messages manually: *To Do*, *Later*, and *Remember*. 

## Add-ons

Here is a list of add-ons that I have installed on thunderbird to extend its basic functionality:
- **Color folders**: This is a simple add-on that lets you colorize the mail folder icons with your preferred colors.
- **Enigmail**: This add-on lets you use OpenPGP encryption and authentication.
- **ExtendIMAPFilters**: Enables filter execution on new IMAP read messages.
- **FireTray**: Provides a system tray icon for thunderbird.
- **Lightning**: Thunderbird's calendar.
- **Inverse SOGo Connector**: It keeps address books and events in sync.
- **Markdown Here**: It transforms an email written in markdown to html.
- **Tag Sequence Arranger**: enables you to arrange the tags.

## Notes

Thunderbird can be also used as a nice notebook that keeps your notes in sync. To accomplish this, I have created another label which is called *Notepad* in Gmail and configured thunderbird to store *template* messages on this folder. In fact, I treat templates as notes. To create a new note, I just create a new message and save it as template. Furthermore, I use the Enigmail extension to encrypt each message before saving it. By doing this, I can use the defined tags to tag my notes, and the search functionality that thunderbird provides for searching.

## Styling

I have made some modifications on the default theme of thunderbird in order to make it look nicer. Most importantly, I changed the behavior of the tags column in order to highlight each tag cell with the color of the most important tag. I have also made some modifications to the background colors of the panels and to the fonts. The figure below is a screenshot of Thunderbird with the new theme applied.

![A screenshot of thunderbird](http://i.imgur.com/odwegMdl.png){: .center-image }

To apply these modifications copy the *chrome* folder of [this](https://github.com/geokapp/colorbird) repository under your thunderbird's profile folder (e.g., *~/.thunderbird/\<profile\>/.* or *~/.icedove/\<profile\>/.* in Debian). Then, restart thunderbird for the new theme to take effect.
